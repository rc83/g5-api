package com.example.model;

import java.sql.Date;

public class UpdateAbstractData {
	
	public UpdateAbstractData() {
		super();
		// TODO Auto-generated constructor stub
	}
	private Integer abstractId;
	private String abstractFileName;
	private Date dateSelected;
	private Date lastModifiedOn;
	private String modifiedUser;
	private String comments;
	private Integer applicationId;
	private String abstractCompleteInd;
	
	public Integer getAbstractId() {
		return abstractId;
	}
	public void setAbstractId(Integer abstractId) {
		this.abstractId = abstractId;
	}
	public String getAbstractFileName() {
		return abstractFileName;
	}
	public void setAbstractFileName(String abstractFileName) {
		this.abstractFileName = abstractFileName;
	}
	public Date getDateSelected() {
		return dateSelected;
	}
	public void setDateSelected(Date dateSelected) {
		this.dateSelected = dateSelected;
	}
	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}
	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}
	public String getModifiedUser() {
		return modifiedUser;
	}
	public void setModifiedUser(String modifiedUser) {
		this.modifiedUser = modifiedUser;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Integer getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}
	public String getAbstractCompleteInd() {
		return abstractCompleteInd;
	}
	public void setAbstractCompleteInd(String abstractCompleteInd) {
		this.abstractCompleteInd = abstractCompleteInd;
	}
	@Override
	public String toString() {
		return "UpdateAbstractData [abstractId=" + abstractId + ", abstractFileName=" + abstractFileName
				+ ", dateSelected=" + dateSelected + ", lastModifiedOn=" + lastModifiedOn + ", modifiedUser="
				+ modifiedUser + ", comments=" + comments + ", applicationId=" + applicationId
				+ ", abstractCompleteInd=" + abstractCompleteInd + "]";
	}
	
}
