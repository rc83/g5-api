package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entity.AbstractInformationEntity;
import com.example.entity.ApplicationInformationEntity;
import com.example.exception.ApplicationNotFoundException;
import com.example.exception.ResourceNotFoundException;
import com.example.model.UpdateAbstractData;
import com.example.repository.AbstractRepository;
import com.example.repository.AppRepository;

@Service
public class ApplicationService {
	
	@Autowired
	private AppRepository appRepository;
	
	@Autowired
	private AbstractRepository absRepository;
	
	/**
	 * Service to return all applications
	 * @return
	 */
	public Iterable<ApplicationInformationEntity> getAllApplications(){		
		return appRepository.findAll();
	}
	
	/**
	 * Service to return all applications for the slate-id
	 * @return
	 */
	public List<ApplicationInformationEntity> getApplicationsForSlate(int id){		
		return appRepository.getAppsBySlateId(id);
	}
	
	/**
	 * Service to return an application.
	 * @return
	 */
	public ApplicationInformationEntity getSingleApplication(int id){		
		return appRepository.findById(id).orElseThrow(() ->new ResourceNotFoundException(id));
	}
	
	/**
	 * Service to return all applications
	 * @return
	 */
	public Iterable<AbstractInformationEntity> getAllAbstracts(){		
		return absRepository.findAll();
	}
	
	/**
	 * Service to delete an abstract.
	 * @return
	 */
	public String deleteAbstract(int abstractId){		
		absRepository.deleteById(abstractId);
		return "Abstract record deleted.";
	}
	
	
	/**
	 * Service to get a single abstract.
	 * @return
	 */
	public AbstractInformationEntity getSingleAbstract(int abstractId){		
		return absRepository.findById(abstractId).orElseThrow(() ->new ResourceNotFoundException(abstractId));
	}


	/**
	 * Update the selected application
	 */
	public ApplicationInformationEntity updateApplicationData(int id, ApplicationInformationEntity appData) {
		
		ApplicationInformationEntity updatedApp = appRepository.findById(id).orElseThrow(() ->new ResourceNotFoundException(id));
		
		updatedApp.setApplicantName(appData.getApplicantName());
		updatedApp.setSlateId(appData.getSlateId());
		updatedApp.setPrAwardNo(appData.getPrAwardNo());
		updatedApp.setRequestedAmount(appData.getRequestedAmount());
		updatedApp.setRecommendedAmount(appData.getRecommendedAmount());
		updatedApp.setTotalAllocatedAmount(appData.getTotalAllocatedAmount());
		updatedApp.setAbstractCompleteInd(appData.getAbstractCompleteInd());
		updatedApp.setAccsDataString(appData.getAccsDataString());
		updatedApp.setGranteeDuns(appData.getGranteeDuns());
		updatedApp.setGrantFundingStatusCd(appData.getGrantFundingStatusCd());
		updatedApp.setEdProgramContact(appData.getEdProgramContact());
		updatedApp.setEdProgramContactEmail(appData.getEdProgramContactEmail());
		updatedApp.setEdProgramContactPhone(appData.getEdProgramContactPhone());
		updatedApp.setStateCd(appData.getStateCd());
		updatedApp.setRankNo(appData.getRankNo());
		updatedApp.setHighRiskCd(appData.getHighRiskCd());
		updatedApp.setApplicationCompleteInd(appData.getApplicationCompleteInd());
		updatedApp.setMsMonitoringReqInd(appData.getMsMonitoringReqInd());
		updatedApp.setMsTechnicalAssistanceInd(appData.getMsTechnicalAssistanceInd());
		updatedApp.setApplicationStatus(appData.getApplicationStatus());
		updatedApp.setNarrativeReceivedInd(appData.getNarrativeReceivedInd());
		updatedApp.setApplicationComment(appData.getApplicationComment());
		updatedApp.setLateInd(appData.getLateInd());
		updatedApp.setDuplicateAward(appData.getDuplicateAward());
		updatedApp.setDuplicateInd(appData.getDuplicateInd());
		updatedApp.setEligibilityStatusCmt(appData.getEligibilityStatusCmt());
		updatedApp.setEligibleStatusCd(appData.getEligibleStatusCd());
		updatedApp.setIneligibleCatCd(appData.getIneligibleCatCd());
		
		appRepository.save(updatedApp);
		
		return updatedApp;
	}
	
	/**
	 * Update the selected abstract entity.
	 */
	public String updateAbstractData(int id, UpdateAbstractData abstractData) {
		// Find the abstract data for the id
		AbstractInformationEntity updatedAbstract = absRepository.findById(id).orElseThrow(() ->new ResourceNotFoundException(id));
		
		// Find the app data to update the indicator
		Integer appId = abstractData.getApplicationId();
		ApplicationInformationEntity appInfoEntity = appRepository.findById(appId).orElseThrow(() -> new ResourceNotFoundException(appId));
		
		appInfoEntity.setAbstractCompleteInd(abstractData.getAbstractCompleteInd());
		
		updatedAbstract.setComments(abstractData.getComments());
		updatedAbstract.setDateSelected(abstractData.getDateSelected());
		updatedAbstract.setLastModifiedOn(abstractData.getLastModifiedOn());
		updatedAbstract.setModifiedUser(abstractData.getModifiedUser());
		updatedAbstract.setAbstractFileName(abstractData.getAbstractFileName());
		
		absRepository.save(updatedAbstract);
		appRepository.save(appInfoEntity);
		
		return "Abstract Data saved.";
	}
}
